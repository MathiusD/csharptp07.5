﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Courtesy;

namespace UnitTestTp07._5
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void HelloWorld()
        {
            Assert.AreEqual("Hello World", Greetings.HelloWorld());
        }
        [TestMethod]
        public void HelloBen()
        {
            Assert.AreEqual("Hello Ben", Greetings.HelloBen());
        }
        [TestMethod]
        public void HelloYou()
        {
            Assert.AreEqual("Hello ", Greetings.HelloYou(""));
            Assert.AreEqual("Hello gseg", Greetings.HelloYou("gseg"));
            Assert.AreEqual("Hello 0101", Greetings.HelloYou("0101"));
        }
    }
}
