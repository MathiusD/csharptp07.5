﻿Imports HelloWorldLib
Imports Courtesy
Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MessageBox.Show(Greetings.HelloWorld())
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Label2.Text() = Greetings.HelloBen()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If (TextBox1.Text <> "") Then
            Label2.Text() = Greetings.HelloYou(TextBox1.Text)
        End If
    End Sub
End Class
