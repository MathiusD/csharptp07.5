﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courtesy
{
    public class Greetings
    {
        public static string HelloWorld()
        {
            return "Hello World";
        }
        public static string HelloBen()
        {
            return "Hello Ben";
        }
        public static string HelloYou(string name)
        {
            return string.Format("Hello {0}", name);
        }
    }
}
